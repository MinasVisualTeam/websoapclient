const express = require("express");
const soap = require('soap');
let xmlParser = require('xml2json');
const { opts } = require('./constants');

const app = express();

app.use(express.json());
app.use(express.static('public'))

const soapServer = (url, credentials) => new Promise((Resolve, Reject) =>{
    return soap.createClient(url,{wsdl_headers: credentials}, (client) => {
       // if(err) Reject(err);
        //client.addSoapHeader(credentials);
        console.log( client.describe() )
        Resolve(client);
    })
});

const main = async (req, res) => {
    let { json, url, method, header } = req.body;
    json = JSON.parse(json);
    const client = await soapServer(url, header);
    console.log("Conn soap success", opts);
    console.time('soap-function');
    const action = method.split('.').reduce((ant, atual) => ant[atual], client);
    //action(json, (err, result) => res.send(xmlParser.toJson(result.body)), opts);
    console.log("Finish function");
    console.timeEnd('soap-function');
};

app.post('/add', main);
app.listen(3000);
console.log('LIstem 3000')