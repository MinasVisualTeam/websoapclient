const opts = {
    proxy: 'http://80582307:Accenture01@proxy.redecorp.br:8080'
};

const URL = "https://svn.apache.org/repos/asf/airavata/sandbox/xbaya-web/test/Calculator.wsdl";

const credentials = {
    AuthenticationMethod:{
        userName: "username",
        password: "password"
    }  
};

module.exports = {
    opts,
    URL,
    credentials
};