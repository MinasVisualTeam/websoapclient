{
    "_events": {},
    "_eventsCount": 0,
    "wsdl": {
        "uri": "https://svn.apache.org/repos/asf/airavata/sandbox/xbaya-web/test/Calculator.wsdl",
        "_includesWsdl": [],
        "WSDL_CACHE": {
            "https://svn.apache.org/repos/asf/airavata/sandbox/xbaya-web/test/Calculator.wsdl": "~wsdl"
        },
        "options": {
            "ignoredNamespaces": [
                "tns",
                "targetNamespace",
                "typedNamespace"
            ],
            "valueKey": "$value",
            "xmlKey": "$xml",
            "escapeXML": true,
            "returnFault": false,
            "handleNilAsNull": false,
            "namespaceArrayElements": true,
            "ignoreBaseNameSpaces": false,
            "forceSoap12Headers": false,
            "useEmptyTag": false,
            "attributesKey": "attributes",
            "envelopeKey": "soap",
            "preserveWhitespace": false
        },
        "definitions": {
            "xmlns": {
                "wsdl": "http://schemas.xmlsoap.org/wsdl/",
                "ns1": "http://org.apache.axis2/xsd",
                "ns": "http://c.b.a",
                "wsaw": "http://www.w3.org/2006/05/addressing/wsdl",
                "http": "http://schemas.xmlsoap.org/wsdl/http/",
                "xs": "http://www.w3.org/2001/XMLSchema",
                "soap": "http://schemas.xmlsoap.org/wsdl/soap/",
                "mime": "http://schemas.xmlsoap.org/wsdl/mime/",
                "soap12": "http://schemas.xmlsoap.org/wsdl/soap12/",
                "__tns__": "http://c.b.a"
            },
            "schemaXmlns": {},
            "valueKey": "$value",
            "xmlKey": "$xml",
            "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
            "$targetNamespace": "http://c.b.a",
            "messages": {
                "addRequest": {
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "$name": "addRequest",
                    "element": {
                        "nsName": "xs:element",
                        "prefix": "xs",
                        "name": "element",
                        "children": [
                            {
                                "nsName": "xs:complexType",
                                "prefix": "xs",
                                "name": "complexType",
                                "children": [
                                    {
                                        "nsName": "xs:sequence",
                                        "prefix": "xs",
                                        "name": "sequence",
                                        "children": [
                                            {
                                                "nsName": "xs:element",
                                                "prefix": "xs",
                                                "name": "element",
                                                "children": [],
                                                "xmlns": {},
                                                "schemaXmlns": {},
                                                "valueKey": "$value",
                                                "xmlKey": "$xml",
                                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                                "$minOccurs": "0",
                                                "$name": "n1",
                                                "$type": "xs:int",
                                                "$targetNamespace": "http://c.b.a"
                                            },
                                            {
                                                "nsName": "xs:element",
                                                "prefix": "xs",
                                                "name": "element",
                                                "children": [],
                                                "xmlns": {},
                                                "schemaXmlns": {},
                                                "valueKey": "$value",
                                                "xmlKey": "$xml",
                                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                                "$minOccurs": "0",
                                                "$name": "n2",
                                                "$type": "xs:int",
                                                "$targetNamespace": "http://c.b.a"
                                            }
                                        ],
                                        "xmlns": {},
                                        "schemaXmlns": {},
                                        "valueKey": "$value",
                                        "xmlKey": "$xml",
                                        "ignoredNamespaces": "~wsdl~options~ignoredNamespaces"
                                    }
                                ],
                                "xmlns": {},
                                "schemaXmlns": {},
                                "valueKey": "$value",
                                "xmlKey": "$xml",
                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces"
                            }
                        ],
                        "xmlns": {},
                        "schemaXmlns": {},
                        "valueKey": "$value",
                        "xmlKey": "$xml",
                        "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                        "$name": "add",
                        "targetNSAlias": "ns",
                        "targetNamespace": "http://c.b.a",
                        "$lookupType": "ns:add",
                        "$lookupTypes": [],
                        "$targetNamespace": "http://c.b.a"
                    },
                    "parts": {
                        "n1": "xs:int",
                        "n2": "xs:int"
                    }
                },
                "addResponse": {
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "$name": "addResponse",
                    "element": {
                        "nsName": "xs:element",
                        "prefix": "xs",
                        "name": "element",
                        "children": [
                            {
                                "nsName": "xs:complexType",
                                "prefix": "xs",
                                "name": "complexType",
                                "children": [
                                    {
                                        "nsName": "xs:sequence",
                                        "prefix": "xs",
                                        "name": "sequence",
                                        "children": [
                                            {
                                                "nsName": "xs:element",
                                                "prefix": "xs",
                                                "name": "element",
                                                "children": [],
                                                "xmlns": {},
                                                "schemaXmlns": {},
                                                "valueKey": "$value",
                                                "xmlKey": "$xml",
                                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                                "$minOccurs": "0",
                                                "$name": "return",
                                                "$type": "xs:int",
                                                "$targetNamespace": "http://c.b.a"
                                            }
                                        ],
                                        "xmlns": {},
                                        "schemaXmlns": {},
                                        "valueKey": "$value",
                                        "xmlKey": "$xml",
                                        "ignoredNamespaces": "~wsdl~options~ignoredNamespaces"
                                    }
                                ],
                                "xmlns": {},
                                "schemaXmlns": {},
                                "valueKey": "$value",
                                "xmlKey": "$xml",
                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces"
                            }
                        ],
                        "xmlns": {},
                        "schemaXmlns": {},
                        "valueKey": "$value",
                        "xmlKey": "$xml",
                        "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                        "$name": "addResponse",
                        "targetNSAlias": "ns",
                        "targetNamespace": "http://c.b.a",
                        "$lookupType": "ns:addResponse",
                        "$lookupTypes": [],
                        "$targetNamespace": "http://c.b.a"
                    },
                    "parts": {
                        "return": "xs:int"
                    }
                },
                "add": "~wsdl~definitions~messages~addRequest"
            },
            "portTypes": {
                "CalculatorPortType": {
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "methods": {
                        "add": {
                            "schemaXmlns": {},
                            "valueKey": "$value",
                            "xmlKey": "$xml",
                            "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                            "$name": "add",
                            "input": "~wsdl~definitions~messages~addRequest~element",
                            "output": "~wsdl~definitions~messages~addResponse~element",
                            "inputSoap": {
                                "schemaXmlns": {},
                                "valueKey": "$value",
                                "xmlKey": "$xml",
                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                "use": "literal"
                            },
                            "outputSoap": {
                                "schemaXmlns": {},
                                "valueKey": "$value",
                                "xmlKey": "$xml",
                                "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                "use": "literal"
                            },
                            "style": "document",
                            "soapAction": "urn:add"
                        }
                    }
                }
            },
            "bindings": {
                "CalculatorSoap11Binding": {
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "transport": "http://schemas.xmlsoap.org/soap/http",
                    "style": "document",
                    "methods": "~wsdl~definitions~portTypes~CalculatorPortType~methods",
                    "topElements": {
                        "add": {
                            "methodName": "add",
                            "outputName": "addResponse"
                        }
                    }
                },
                "CalculatorSoap12Binding": {
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "transport": "http://schemas.xmlsoap.org/soap/http",
                    "style": "document",
                    "methods": "~wsdl~definitions~portTypes~CalculatorPortType~methods",
                    "topElements": {
                        "add": {
                            "methodName": "add",
                            "outputName": "addResponse"
                        }
                    }
                }
            },
            "services": {
                "Calculator": {
                    "children": [
                        {
                            "nsName": "wsdl:port",
                            "prefix": "wsdl",
                            "name": "port",
                            "children": [
                                {
                                    "nsName": "http:address",
                                    "prefix": "http",
                                    "name": "address",
                                    "children": [],
                                    "xmlns": {},
                                    "schemaXmlns": {},
                                    "valueKey": "$value",
                                    "xmlKey": "$xml",
                                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                    "$location": "https://156.56.179.164:9443/services/Calculator.CalculatorHttpsEndpoint/"
                                }
                            ],
                            "xmlns": {},
                            "schemaXmlns": {},
                            "valueKey": "$value",
                            "xmlKey": "$xml",
                            "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                            "$name": "CalculatorHttpsEndpoint",
                            "$binding": "ns:CalculatorHttpBinding",
                            "location": "https://156.56.179.164:9443/services/Calculator.CalculatorHttpsEndpoint/"
                        },
                        {
                            "nsName": "wsdl:port",
                            "prefix": "wsdl",
                            "name": "port",
                            "children": [
                                {
                                    "nsName": "http:address",
                                    "prefix": "http",
                                    "name": "address",
                                    "children": [],
                                    "xmlns": {},
                                    "schemaXmlns": {},
                                    "valueKey": "$value",
                                    "xmlKey": "$xml",
                                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                                    "$location": "http://156.56.179.164:9763/services/Calculator.CalculatorHttpEndpoint/"
                                }
                            ],
                            "xmlns": {},
                            "schemaXmlns": {},
                            "valueKey": "$value",
                            "xmlKey": "$xml",
                            "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                            "$name": "CalculatorHttpEndpoint",
                            "$binding": "ns:CalculatorHttpBinding",
                            "location": "http://156.56.179.164:9763/services/Calculator.CalculatorHttpEndpoint/"
                        }
                    ],
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "ports": {
                        "CalculatorHttpsSoap11Endpoint": {
                            "location": "https://156.56.179.164:9443/services/Calculator.CalculatorHttpsSoap11Endpoint/",
                            "binding": "~wsdl~definitions~bindings~CalculatorSoap11Binding"
                        },
                        "CalculatorHttpSoap11Endpoint": {
                            "location": "http://156.56.179.164:9763/services/Calculator.CalculatorHttpSoap11Endpoint/",
                            "binding": "~wsdl~definitions~bindings~CalculatorSoap11Binding"
                        },
                        "CalculatorHttpSoap12Endpoint": {
                            "location": "http://156.56.179.164:9763/services/Calculator.CalculatorHttpSoap12Endpoint/",
                            "binding": "~wsdl~definitions~bindings~CalculatorSoap12Binding"
                        },
                        "CalculatorHttpsSoap12Endpoint": {
                            "location": "https://156.56.179.164:9443/services/Calculator.CalculatorHttpsSoap12Endpoint/",
                            "binding": "~wsdl~definitions~bindings~CalculatorSoap12Binding"
                        }
                    }
                }
            },
            "schemas": {
                "http://c.b.a": {
                    "nsName": "xs:schema",
                    "prefix": "xs",
                    "name": "schema",
                    "children": [],
                    "xmlns": {
                        "__tns__": "http://c.b.a"
                    },
                    "schemaXmlns": {},
                    "valueKey": "$value",
                    "xmlKey": "$xml",
                    "ignoredNamespaces": "~wsdl~options~ignoredNamespaces",
                    "$attributeFormDefault": "qualified",
                    "$elementFormDefault": "qualified",
                    "$targetNamespace": "http://c.b.a",
                    "complexTypes": {},
                    "types": {},
                    "elements": {
                        "add": "~wsdl~definitions~messages~addRequest~element",
                        "addResponse": "~wsdl~definitions~messages~addResponse~element"
                    },
                    "includes": []
                }
            },
            "descriptions": {
                "types": {}
            }
        },
        "xml": "<wsdl:definitions xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:ns1=\"http://org.apache.axis2/xsd\" xmlns:ns=\"http://c.b.a\" xmlns:wsaw=\"http://www.w3.org/2006/05/addressing/wsdl\" xmlns:http=\"http://schemas.xmlsoap.org/wsdl/http/\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:mime=\"http://schemas.xmlsoap.org/wsdl/mime/\" xmlns:soap12=\"http://schemas.xmlsoap.org/wsdl/soap12/\" targetNamespace=\"http://c.b.a\">\n    <wsdl:documentation>Calculator</wsdl:documentation>\n    <wsdl:types>\n        <xs:schema attributeFormDefault=\"qualified\" elementFormDefault=\"qualified\" targetNamespace=\"http://c.b.a\">\n            <xs:element name=\"add\">\n                <xs:complexType>\n                    <xs:sequence>\n                        <xs:element minOccurs=\"0\" name=\"n1\" type=\"xs:int\" />\n                        <xs:element minOccurs=\"0\" name=\"n2\" type=\"xs:int\" />\n                    </xs:sequence>\n                </xs:complexType>\n            </xs:element>\n            <xs:element name=\"addResponse\">\n                <xs:complexType>\n                    <xs:sequence>\n                        <xs:element minOccurs=\"0\" name=\"return\" type=\"xs:int\" />\n                    </xs:sequence>\n                </xs:complexType>\n            </xs:element>\n        </xs:schema>\n    </wsdl:types>\n    <wsdl:message name=\"addRequest\">\n        <wsdl:part name=\"parameters\" element=\"ns:add\" />\n    </wsdl:message>\n    <wsdl:message name=\"addResponse\">\n        <wsdl:part name=\"parameters\" element=\"ns:addResponse\" />\n    </wsdl:message>\n    <wsdl:portType name=\"CalculatorPortType\">\n        <wsdl:operation name=\"add\">\n            <wsdl:input message=\"ns:addRequest\" wsaw:Action=\"urn:add\" />\n            <wsdl:output message=\"ns:addResponse\" wsaw:Action=\"urn:addResponse\" />\n        </wsdl:operation>\n    </wsdl:portType>\n    <wsdl:binding name=\"CalculatorSoap11Binding\" type=\"ns:CalculatorPortType\">\n        <soap:binding transport=\"http://schemas.xmlsoap.org/soap/http\" style=\"document\" />\n        <wsdl:operation name=\"add\">\n            <soap:operation soapAction=\"urn:add\" style=\"document\" />\n            <wsdl:input>\n                <soap:body use=\"literal\" />\n            </wsdl:input>\n            <wsdl:output>\n                <soap:body use=\"literal\" />\n            </wsdl:output>\n        </wsdl:operation>\n    </wsdl:binding>\n    <wsdl:binding name=\"CalculatorSoap12Binding\" type=\"ns:CalculatorPortType\">\n        <soap12:binding transport=\"http://schemas.xmlsoap.org/soap/http\" style=\"document\" />\n        <wsdl:operation name=\"add\">\n            <soap12:operation soapAction=\"urn:add\" style=\"document\" />\n            <wsdl:input>\n                <soap12:body use=\"literal\" />\n            </wsdl:input>\n            <wsdl:output>\n                <soap12:body use=\"literal\" />\n            </wsdl:output>\n        </wsdl:operation>\n    </wsdl:binding>\n    <wsdl:binding name=\"CalculatorHttpBinding\" type=\"ns:CalculatorPortType\">\n        <http:binding verb=\"POST\" />\n        <wsdl:operation name=\"add\">\n            <http:operation location=\"add\" />\n            <wsdl:input>\n                <mime:content type=\"text/xml\" part=\"parameters\" />\n            </wsdl:input>\n            <wsdl:output>\n                <mime:content type=\"text/xml\" part=\"parameters\" />\n            </wsdl:output>\n        </wsdl:operation>\n    </wsdl:binding>\n    <wsdl:service name=\"Calculator\">\n        <wsdl:port name=\"CalculatorHttpsSoap11Endpoint\" binding=\"ns:CalculatorSoap11Binding\">\n            <soap:address location=\"https://156.56.179.164:9443/services/Calculator.CalculatorHttpsSoap11Endpoint/\" />\n        </wsdl:port>\n        <wsdl:port name=\"CalculatorHttpSoap11Endpoint\" binding=\"ns:CalculatorSoap11Binding\">\n            <soap:address location=\"http://156.56.179.164:9763/services/Calculator.CalculatorHttpSoap11Endpoint/\" />\n        </wsdl:port>\n        <wsdl:port name=\"CalculatorHttpSoap12Endpoint\" binding=\"ns:CalculatorSoap12Binding\">\n            <soap12:address location=\"http://156.56.179.164:9763/services/Calculator.CalculatorHttpSoap12Endpoint/\" />\n        </wsdl:port>\n        <wsdl:port name=\"CalculatorHttpsSoap12Endpoint\" binding=\"ns:CalculatorSoap12Binding\">\n            <soap12:address location=\"https://156.56.179.164:9443/services/Calculator.CalculatorHttpsSoap12Endpoint/\" />\n        </wsdl:port>\n        <wsdl:port name=\"CalculatorHttpsEndpoint\" binding=\"ns:CalculatorHttpBinding\">\n            <http:address location=\"https://156.56.179.164:9443/services/Calculator.CalculatorHttpsEndpoint/\" />\n        </wsdl:port>\n        <wsdl:port name=\"CalculatorHttpEndpoint\" binding=\"ns:CalculatorHttpBinding\">\n            <http:address location=\"http://156.56.179.164:9763/services/Calculator.CalculatorHttpEndpoint/\" />\n        </wsdl:port>\n    </wsdl:service>\n</wsdl:definitions>",
        "services": "~wsdl~definitions~services",
        "xmlnsInEnvelope": " xmlns:ns1=\"http://org.apache.axis2/xsd\" xmlns:ns=\"http://c.b.a\""
    },
    "Calculator": {
        "CalculatorHttpsSoap11Endpoint": {},
        "CalculatorHttpSoap11Endpoint": {},
        "CalculatorHttpSoap12Endpoint": {},
        "CalculatorHttpsSoap12Endpoint": {}
    },
    "httpClient": {}
}